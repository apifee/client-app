const _           = require('lodash');
const express    	= require('express');
const app        	= express();
const abi        	= require("./contract-definition.json");
const Promise 	 	= require('bluebird');
const bodyParser 	= require('body-parser')
const apifeeFactory = require('../sdk-node').factory;
const logos 		= require('./images');

var agent = require('superagent-promise')(require('superagent'), Promise);

//// -----------------

const ethereumProvider            = 'http://localhost:8545';
const apiUrl 		   			          = 'http://localhost:3001/api/make?text=';
const subscriptionContractAddress = _.get(process.env, 'CONTRACT_ADDRESS', '');
const privateKey 	   			        = '0x636fda68f83960fbea5521cc24dfcc63982440c7c732aa7acd4ce1e869f6f335';

const quotes =
[
  "                All   your   tokens   are   belong   to   us   ",
  "                                   I   accidentally   killed   it           ",
  "                                  BUIDL   the   community			  ",
  "                    Anyone   can   kill   your   contract       ",
]

//// -----------------

let web3;
let channelId;
let apifee;

const server = app.listen(3002, function() {
    console.log("We have started our server on port 3002");
    console.log('Contract address', subscriptionContractAddress);

    apifee = apifeeFactory(
    abi,
    ethereumProvider,
    privateKey,
    subscriptionContractAddress,
    console
  );

  apifee.initializeClient();
});

app.use(bodyParser.json())

app.get('/ethbuenosaires',function(req, res) {
  const apifeeHeaders = apifee.getNextRequestHeaders();
  const targetRequest = agent('GET', apiUrl + quotes[getRandomInt(quotes.length)]);

  for (let key in apifeeHeaders) {
    targetRequest.set(key, apifeeHeaders[key]);
  }
  // console.log('Hitting API with headers:');
  // console.log(targetRequest.header);

  targetRequest
    .then(function onResult(proxiedRes) {
        let response;
        if (proxiedRes.ok) {
          response = logos.ethereum + "\n\n\n\n\n" + proxiedRes.text;
        }
        else {
          response.error = proxiedRes.error;
        }
        res.setHeader('Content-Type','text/plain');
        res.send(response);
      }).catch(function(err){
          if (err.status === 402) {
            apifee.rollbackCreditCount();
          }

          console.log(err);
          res.status(err.status).json(err.response.body);
      });
});

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
